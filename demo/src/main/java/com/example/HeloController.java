package com.example;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HeloController {
	//入力画面準備
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView index(ModelAndView mav){
		mav.setViewName("index");
		mav.addObject("val","please type...");
		return mav;//入力画面表示
	}
	@RequestMapping(value="/",method=RequestMethod.POST)//入力画面からのsubmitを受け取る
	public ModelAndView conform(@RequestParam String adress,ModelAndView mav){
		
		mav.setViewName("conform");//遷移先である確認画面jspの指定
		mav.addObject("adress",adress);//valを確認画面に送る
		mav.addObject("val","you type:'"+adress+"'.");//valを確認画面に送る
		return mav;//確認画面に遷移
	}
	
	@Autowired
    DemoRepository repository;
	@RequestMapping(value="/conform",method=RequestMethod.POST)//確認画面からのsubmitを受け取る
	public ModelAndView complete(@RequestParam String adress,ModelAndView mav){
		Demodb demodb = new Demodb(adress);
		repository.saveAndFlush(demodb);
		Iterable<Demodb> list = repository.findAll();
		mav.setViewName("complete");//遷移先である確認画面jspの指定
		mav.addObject("results", list);
		return mav;//完了画面に遷移
	}
	@RequestMapping(value="/topback",method=RequestMethod.POST)//確認画面からのsubmitを受け取る
	public ModelAndView topback(@RequestParam String topback,ModelAndView mav){
		return new ModelAndView("redirect:/");//確認画面に遷移
	}
}
