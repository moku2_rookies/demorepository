package com.example;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Demodb {
	@Id
    @GeneratedValue
    protected Integer id;
    protected String adress;

    public Demodb() {
      super();
    }

    public Demodb(String adress) {

      super();
      this.adress = adress;
    }

    // for debug
    public String toString() {
      return "[adress:" + adress + "]";
    }
}
